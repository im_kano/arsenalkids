
$("#menu-share").animatedModal();
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.navbar').outerHeight();

$(window).scroll(function(event) {
  didScroll = true;
  var offdiv = $('.navbar');
  var offdivalue = $(window).scrollTop();
  if (offdivalue == 0) {
    $('.right-logo').css('background-color', 'transparent');
  };

  $('.hideme').each(function(i) {
    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    if (bottom_of_window > bottom_of_object) {
      $(this).animate({
        'opacity': '1'
      }, 600);

    }
  });
});

setInterval(function() {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 250);

function hasScrolled() {
  var st = $(this).scrollTop();
  if (Math.abs(lastScrollTop - st) <= delta)
    return;
  if (st > lastScrollTop && st > navbarHeight) {
    // Scroll Down
    $('.navbar').removeClass('nav-down').addClass('nav-up');
    $('.right-logo').css('background-color', 'rgba(0, 0, 0, 0.7)');
  } else {
    // Scroll Up
    if (st + $(window).height() < $(document).height()) {
      $('.navbar').removeClass('nav-up').addClass('nav-down');
    }
  }
  lastScrollTop = st;
}

$(document).ready(function() {
  if (window.innerWidth < 768) {
    $('.content-1-cels').slick({
      dots: true,
      arrows: true,
      appendArrows: $('.arrowsSlider'),
      prevArrow: '<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/png/32/chevron-left.png">',
      nextArrow: '<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/png/32/chevron-right.png">',
      infinite: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [{
        breakpoint: 650,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        }
      }]
    });
  }
  $('#slider2').slick({
    appendArrows: $('.arrowsSlider2'),
    prevArrow: '<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/png/32/chevron-left.png">',
    nextArrow: '<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/png/32/chevron-right.png">',
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 7,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        }
      }
  ]
  });



$('.fly-button').hover(function() {
  $('.notactive').addClass('animate').removeClass('notactive');
  $('a#menu-share + ul').css('visibility', 'visible');
}, function() {
  $('.animate').removeClass('animate').addClass('notactive');
  $('a#menu-share + ul').css('visibility', 'hidden');
});


  siteFooter();

	$(window).resize(function() {
		siteFooter();
	});

	function siteFooter() {
		var siteContent = $('#site-content');
		var siteContentHeight = siteContent.height();
		var siteContentWidth = siteContent.width();

		var siteFooter = $('#site-footer');
		var siteFooterHeight = siteFooter.height();
		var siteFooterWidth = siteFooter.width();
		siteContent.css({
			"margin-bottom" : siteFooterHeight + 50
		});
	};


  $(".slide-click").click(function(e) {
  e.preventDefault();
  var aid = $(this).attr("href");
   $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
});

$('#scrlBotm').click(function () {
  $('html, body').animate({
      scrollTop: $(document).height()
  }, 1500);
  return false;
});
$('#scrBot').click(function () {
  $('html, body').animate({
      scrollTop: $(document).height()
  }, 1500);
  return false;
});

});
